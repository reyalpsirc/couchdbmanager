﻿# Description:

CouchDBManager is an utility that provide some extra operations not found on Futon and that are handy when you're developing a CouchDB application. 
You can run it on Windows and OS X (with `mono`). It should also be possible to run it on Linux but I did not test it yet.
The current operations supported by this are:

* **Bulk delete** operations (which first creates a backup of the files to delete).
* **Backup** of all database or of a specific design view (through it's url path) to a specific location
* **Restore** of a previous backup made through CouchDBManager. This option also allows you to restore only the docs of the backup that are deleted on your server and to restore only design documents if needed.
* Simple **Statistics** of the documents found at a specific path on a database. That path might be `_all_docs?include_docs=true`

# How to install:

Check the [Downloads](https://bitbucket.org/reyalpsirc/couchdbmanager/downloads) section for the latest version binary. Unzip the project to your desired location.

# Examples of usage:

Delete the files returned by a view:

`/location/to/CouchDBManager.exe delete http://127.0.0.1:5984/ projectDB _design/user/_view/userStuff --username admin --password mypass`

Backup the entire database:

`/location/to/CouchDBManager.exe backup /a/destination/folder/ ALL http://127.0.0.1:5984/ projectDB --username admin --password mypass`

Backup of the files returned from a design view of the database:

`/location/to/CouchDBManager.exe backup /a/destination/folder/ _design/user/_view/userStuff http://127.0.0.1:5984/ projectDB --username admin --password mypass`

Restore a backup:

`/location/to/CouchDBManager.exe restore /path/to/backup.json http://127.0.0.1:5984/ projectDB --username admin --password mypass`

Statistics of a specific design view:

`/location/to/CouchDBManager.exe statistics http://127.0.0.1:5984/ projectDB _design/user/_view/userStuff --username admin --password mypass`

You can also see the help of a command by writing help [command] like this example:

`/location/to/CouchDBManager.exe help backup`

# Notes

For OS X, add the `mono` keyword at the beggining of the command. Example:

`mono /location/to/CouchDBManager.exe backup /a/destination/folder/ http://127.0.0.1:5984/ projectDB --username admin --password mypass`