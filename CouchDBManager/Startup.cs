﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace CouchDBManager
{
	public class Startup
	{
		public static OrderedDictionary operations=new OrderedDictionary(){
			{"delete", new DeleteOperation ()},
			{"backup", new BackupOperation ()},
			{"restore", new RestoreOperation ()},
			{"statistics", new StatisticsOperation ()},
			{"version", new VersionOperation ()},
			{"help", new HelpOperation ()}
		};

		public static void Main (string[] args)
		{
			string command = "help";
			if (args.Length > 0) {
				if (operations.Contains (args [0])) {
					command = args [0];
				} else {
					Console.WriteLine (string.Format("Invalid operation specified: {0}.",args [0]));
					Console.WriteLine ("\nAvailable options:");
					HelpOperation help = new HelpOperation ();
					help.Execute ();
					return;
				}
			}
			var theOperation = (OperationAbstract)operations [command];
			if (args.Length - 1 < theOperation.MinimumRequiredArgs ()) {
				WriteCommandHelp (theOperation);
			} else if (theOperation.ValidateArgs (args)) {
				theOperation.Execute ();
			}
				
		}

		public static void WriteCommandHelp (OperationAbstract theOperation)
		{
			Console.WriteLine ("Usage: "+theOperation.Help());
			var paramsDesc = theOperation.ParamsDescription ();
			if (paramsDesc != null) {
				Console.WriteLine ("\n");
				foreach (var parameter in paramsDesc.Keys) {
					Console.WriteLine ("    "+parameter+": "+paramsDesc[parameter]+"\n");
				}
			}
		}
	}
}

