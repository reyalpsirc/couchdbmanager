﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CouchDBManager
{
	public class DeleteOperation:OperationAbstract
	{
		string host,database,path,username,password;

		public DeleteOperation ()
		{
		}

		public override string Help ()
		{
			return "CouchDbManager delete HOST DATABASE PATH [-u USER  -p PASSWORD]";
		}

		public override Dictionary<string, string> ParamsDescription ()
		{
			return new Dictionary<string, string> () {
				{ "HOST","The address where couchdb is running." },
				{ "DATABASE","The database to access." }, 
				{ "PATH", "The path that will return the documents to delete. This can be found from the urls on the browser. Example: _all_docs?include_docs=true"},
				{ "-u or --username","Username for authentication on the selected database" },
				{ "-p or --password","Password of the given user" }
			};
		}


		public override int MinimumRequiredArgs ()
		{
			return 3;
		}

		public override void Execute ()
		{
			CouchConnection con = new CouchConnection (host,database,username,password);
			//try to get a list of documents at the specified path
			JArray data = con.GetDocsListAtPath(path);
			if (data != null) {
				//if its not null, then the path contains a list of docs to delete
				if (data.Count > 0) {
					Console.WriteLine (string.Format ("Confirm delete of {0} document(s)? Y/N", data.Count));
					string confirm = Console.ReadLine ();
					if (confirm.ToLower () == "y") {
						//create backup
						var docs=new JObject();
						docs ["docs"] = data;
						BackupOperation backup = new BackupOperation ();
						backup.WriteBackup (System.IO.Path.GetDirectoryName (System.Reflection.Assembly.GetEntryAssembly ().Location), docs.ToString ());
						//convert JArray to List
						List<JObject> docsToDelete=new List<JObject>();
						foreach (var d in data) {
							JObject theObj = null;
							//doc is used on _all_docs?include_docs=true
							if (d ["doc"] != null && d ["doc"].ToString () != "") {
								theObj = (JObject)d ["doc"];
							} else if (d ["value"] != null && d ["value"].ToString () != "") {
								theObj = (JObject)d ["value"];
							}
							if (theObj != null) {
								//add the doc to the array of docs to delete
								docsToDelete.Add(theObj);
							}
						}
						DeletListOfDocs (docsToDelete, con);
					}
				}
			} else {
				//let's check if the path contains one document only
				JObject singleDoc=con.GetDocAtPath(path);
				if (singleDoc != null) {
					Console.WriteLine (string.Format ("Confirm delete of document {0}? Y/N", singleDoc["_id"]));
					string confirm = Console.ReadLine ();
					if (confirm.ToLower () == "y") {
						//create backup
						var docs = new JObject ();
						var theArr = new JArray ();
						theArr.Add (singleDoc);
						docs ["docs"] = theArr;
						BackupOperation backup = new BackupOperation ();
						backup.WriteBackup (System.IO.Path.GetDirectoryName (System.Reflection.Assembly.GetEntryAssembly ().Location), docs.ToString ());
						//Delete the only document found at the path
						List<JObject> docsToDelete=new List<JObject>();
						docsToDelete.Add (singleDoc);
						DeletListOfDocs (docsToDelete, con);
					}
				} else {
					Console.WriteLine ("No documents found at the specified path");
				}
			}
		}

		private void DeletListOfDocs(List<JObject> docsToDelete, CouchConnection con){
			List<string> finishErrors = new List<string> ();
			con.RetryConflictsOnBulkSend (delegate() {
				//grab the latest revision of each doc of the server
				Dictionary<string, string> existentDocIdsWithRev = con.GetExistentIdsWithRevs ();
				//create the list of documents to delete
				List<JObject> realListOfDocs=new List<JObject>();
				foreach (var item in docsToDelete) {
					if (item ["_id"] != null && item ["_id"].ToString () != "") {
						var docId = item ["_id"].ToString ();
						//Only delete docs if they exist on the server
						if (existentDocIdsWithRev.ContainsKey (docId)){
							JObject deleteDocData=new JObject();
							deleteDocData["_id"]=docId;
							deleteDocData ["_rev"] = existentDocIdsWithRev [docId];
							deleteDocData ["_deleted"]=true;
							realListOfDocs.Add(deleteDocData);
						}
					}
				}
				return realListOfDocs;
			}, delegate(BulkFails failResults) {
				//store the errors that are not conflicts to show to the user
				if (failResults.OtherIdsErrors!=null && failResults.OtherIdsErrors.Count>0){
					foreach (var item in failResults.OtherIdsErrors) {
						if (item.Doc!=null && item.Doc["_id"]!=null && item.Doc["_id"].ToString()!=""){
							finishErrors.Add("Failed for \""+item.Doc["_id"].ToString()+"\": "+item.Error);
						}
					}
				}
				//replace the value of docsToDelete in order to only retry the operation for the conflicted docs
				if (failResults.ConflictedDocs!=null && failResults.ConflictedDocs.Count>0){
					docsToDelete=failResults.ConflictedDocs;
				}
			});
			foreach (var error in finishErrors) {
				Console.WriteLine(error);
			}
		}

		public override bool ValidateArgs (string[] args)
		{

			host = ParseStringArgs(args,1);
			database = ParseStringArgs(args,2);
			path = ParseStringArgs(args,3);
			username = ParseStringArgs(args,"username","u");
			password = ParseStringArgs(args,"password","p");

			if (username!=null && password==null) {
				Console.WriteLine (string.Format("Please provide password for {0}.",username));
				return false;
			}
			return true;
		}

	}
}

