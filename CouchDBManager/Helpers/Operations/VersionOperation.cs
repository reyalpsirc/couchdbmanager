﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace CouchDBManager
{
	public class VersionOperation:OperationAbstract
	{
		public VersionOperation ()
		{
		}

		public override string Help ()
		{
			return "CouchDbManager version";
		}

		public override void Execute ()
		{
			Console.WriteLine ("Version: " + Assembly.GetExecutingAssembly().GetName().Version);
		}

		public override bool ValidateArgs (string[] args)
		{
			return true;
		}

		public override Dictionary<string, string> ParamsDescription ()
		{
			return null;
		}

		public override int MinimumRequiredArgs ()
		{
			return 0;
		}
	}
}

