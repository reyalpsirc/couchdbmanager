﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace CouchDBManager
{
	public class RestoreOperation:OperationAbstract
	{
		string backup_file,host,database,path,username,password;

		public enum RestoreType
		{
			AllDocs=1,
			DeletedDocs=2,
			AllNonDesignDocs=3,
			DeletedNonDesignDocs=4,
			AllDesignDocs=5,
			DeletedDesignDocs=6
		}

		public RestoreOperation ()
		{
		}

		public override string Help ()
		{
			return "CouchDbManager restore BACKUP_FILE HOST DATABASE [-u USER  -p PASSWORD]";
		}

		public override Dictionary<string, string> ParamsDescription ()
		{
			return new Dictionary<string, string> () {
				{ "BACKUP_FILE", "The location of the backup file to restore."},
				{ "HOST","The address where couchdb is running." },
				{ "DATABASE","The database to access." }, 
				{ "-u or --username","Username for authentication on the selected database" },
				{ "-p or --password","Password of the given user" }
			};
		}
		public override int MinimumRequiredArgs ()
		{
			return 3;
		}

		public override bool ValidateArgs (string[] args)
		{

			backup_file = ParseStringArgs(args,1);
			host = ParseStringArgs(args,2);
			database = ParseStringArgs(args,3);
			username = ParseStringArgs(args,"username","u");
			password = ParseStringArgs(args,"password","p");

			if (username!=null && password==null) {
				Console.WriteLine (string.Format("Please provide password for {0}.",username));
				return false;
			}

			//confirm the backup file exists
			if (!File.Exists (backup_file)) {
				Console.WriteLine ("Backup file \"" + backup_file + "\" does not exist");
				return false;
			}
			return true;
		}

		public override void Execute ()
		{
			//parse the backup file
			BackupParsedDocs bkpDocs=ParseBackupFile(backup_file);
			if (bkpDocs==null){
				Console.WriteLine ("The backup file is not valid or is corrupt");
				return;
			}

			//check if there's documents on the backup file
			if (bkpDocs.designDocs.Count == 0 && bkpDocs.normalDocs.Count == 0) {
				Console.WriteLine ("No documents on the backup to restore.");
				return;
			}

			//create the CouchConnection instance
			CouchConnection con = new CouchConnection (host,database,username,password);

			//get the restore type and execute it
			var operationType = AskForRestoreType (bkpDocs);
			ExecuteOperationForType (bkpDocs,operationType,con);
		}

		RestoreType AskForRestoreType (BackupParsedDocs bkpDocs)
		{
			Console.WriteLine ("Please choose an option:");
			List<int> allOptions = new List<int> (){ 1, 2 };
			Console.WriteLine ("1: Restore ALL documents of the backup.");
			Console.WriteLine ("2: Restore ONLY documents of the backup that were DELETED on server.");
			if (bkpDocs.normalDocs.Count > 0) {
				allOptions.Add (3);
				allOptions.Add (4);
				Console.WriteLine ("3: Restore ALL NON DESIGN documents of the backup.");
				Console.WriteLine ("4: Restore ONLY NON DESIGN documents of the backup that were DELETED on server.");
			}
			if (bkpDocs.designDocs.Count > 0) {
				allOptions.Add (5);
				allOptions.Add (6);
				Console.WriteLine ("5: Restore ALL DESIGN documents of the backup.");
				Console.WriteLine ("6: Restore ONLY DESIGN documents of the backup that were DELETED on server.");
			}
			Console.WriteLine ("\nNOTE: The revision of the restored files will be different from the revision on the backup documents.");
			int option = -1;
			do {
				string confirm = Console.ReadLine ();
				int.TryParse(confirm,out option);
			} while (!allOptions.Contains(option));
			Console.WriteLine ("");
			return (RestoreType)option;
		}

		private void ExecuteOperationForType (BackupParsedDocs bkpDocs, RestoreType operationType, CouchConnection con)
		{
			switch (operationType) {
			case RestoreType.AllDocs:
				RestoreList(con,bkpDocs.normalDocs.Concat (bkpDocs.designDocs).ToList());
				break;
			case RestoreType.DeletedDocs:
				RestoreList(con,bkpDocs.normalDocs.Concat (bkpDocs.designDocs).ToList(),true);
				break;
			case RestoreType.AllNonDesignDocs:
				RestoreList(con,bkpDocs.normalDocs);
				break;
			case RestoreType.DeletedNonDesignDocs:
				RestoreList(con,bkpDocs.normalDocs,true);
				break;
			case RestoreType.AllDesignDocs:
				RestoreList(con,bkpDocs.designDocs);
				break;
			case RestoreType.DeletedDesignDocs:
				RestoreList(con,bkpDocs.designDocs,true);
				break;
			}
		}

		void RestoreList (CouchConnection con, List<JObject> documentsToRestore, bool onlyDeleted=false)
		{
			List<string> finishErrors = new List<string> ();
			con.RetryConflictsOnBulkSend (delegate() {
				//grab the latest revision of each doc of the server
				Dictionary<string, string> existentDocIdsWithRev = con.GetExistentIdsWithRevs ();
				//create the list of documents to restore based on the onlyDeleted flag
				List<JObject> realListOfDocs=new List<JObject>();
				foreach (var item in documentsToRestore) {
					if (item ["_id"] != null && item ["_id"].ToString () != "") {
						var docId = item ["_id"].ToString ();
						if (!existentDocIdsWithRev.ContainsKey (docId)){
							//if they do not exist on the server, we insert them whether onlyDeleted is true or false
							if (item ["_rev"] != null && item ["_rev"].ToString () != "") {
								item.Remove ("_rev");
							}
							realListOfDocs.Add(item);
						} else if (!onlyDeleted){
							//if they EXIST on the server, we only insert them if onlyDeleted is false (which means "insert all files")
							item ["_rev"] = existentDocIdsWithRev [docId];
							realListOfDocs.Add(item);
						}
					}
				}
				return realListOfDocs;
			}, delegate(BulkFails failResults) {
				//store the errors that are not conflicts to show to the user
				if (failResults.OtherIdsErrors!=null && failResults.OtherIdsErrors.Count>0){
					foreach (var item in failResults.OtherIdsErrors) {
						if (item.Doc!=null && item.Doc["_id"]!=null && item.Doc["_id"].ToString()!=""){
							finishErrors.Add("Failed for \""+item.Doc["_id"].ToString()+"\": "+item.Error);
						}
					}
				}
				//replace the value of documentsToRestore in order to only retry the operation for the conflicted docs
				if (failResults.ConflictedDocs!=null && failResults.ConflictedDocs.Count>0){
					documentsToRestore=failResults.ConflictedDocs;
				}
			});
			foreach (var error in finishErrors) {
				Console.WriteLine(error);
			}
		}

		/// <summary>
		/// Parses the backup file in order to get the documents of it
		/// </summary>
		/// <returns>An object with design documents and other documents.</returns>
		/// <param name="path">path to the backup file.</param>
		private BackupParsedDocs ParseBackupFile (string path)
		{
			//get the contents of the file
			string fileContents=File.ReadAllText(path);

			//check if the file is in JSON format
			JObject json = null;
			try {
				json = JObject.Parse (fileContents);
			} catch (Exception ex){
				json = null;
			}
			if (json == null) {
				Console.WriteLine ("The backup file is not valid or is corrupt");
				return null;
			}

			BackupParsedDocs bkpDocs = new BackupParsedDocs ();
			try {
				if (json["docs"]==null || json["docs"].ToString()==""){
					return null;
				}
				JArray docs=(JArray)json ["docs"];
				foreach (JObject docData in docs){
					JObject doc=null;
					if (docData["doc"]!=null && docData["doc"]["_id"]!=null){
						doc=(JObject)docData["doc"];
					} else if (docData["value"]!=null && docData["value"]["_id"]!=null){
						doc=(JObject)docData["value"];
					}
					if (doc!=null){
						string id=doc["_id"].ToString();
						if (id.StartsWith("_design/")){
							bkpDocs.designDocs.Add(doc);
						} else {
							bkpDocs.normalDocs.Add(doc);
						}
					}
				}
				Console.WriteLine(string.Format("Found {0} design document(s)",bkpDocs.designDocs.Count));
				Console.WriteLine(string.Format("Found {0} other document(s)\n",bkpDocs.normalDocs.Count));
			} catch (Exception ex){
				return null;
			}
			return bkpDocs;
		}

	}
}

