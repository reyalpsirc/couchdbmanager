﻿using System;
using System.Collections.Generic;

namespace CouchDBManager
{
	public abstract class OperationAbstract
	{

		public abstract string Help();

		public abstract Dictionary<string,string> ParamsDescription();

		public abstract int MinimumRequiredArgs();

		public abstract void Execute();

		public abstract bool ValidateArgs(string[] args);

		protected string ParseStringArgs (string[] args, int i)
		{
			return i<args.Length?args [i]:null;
		}

		protected string ParseStringArgs (string[] args, string argumentName)
		{
			string value = null;
			int indexOfArg = Array.IndexOf (args, "--" + argumentName);
			if (indexOfArg > -1 && (indexOfArg+1)<args.Length) {
				value = args [indexOfArg + 1];
			}
			return value;
		}

		protected bool ParseBoolArgs (string[] args, string argumentName)
		{
			int indexOfArg = Array.IndexOf (args, "--" + argumentName);
			return indexOfArg > -1;
		}

		protected string ParseStringArgs (string[] args, string argumentName, string mnemonic)
		{
			string value = ParseStringArgs (args, argumentName);
			if (value == null) {
				int indexOfArg = Array.IndexOf (args, "-" + mnemonic);
				if (indexOfArg > -1 && (indexOfArg+1)<args.Length) {
					value = args [indexOfArg + 1];
				}
			}
			return value;
		}

		protected bool ParseBoolArgs (string[] args, string argumentName, string mnemonic)
		{
			bool value = ParseBoolArgs (args, argumentName);
			int indexOfArg = Array.IndexOf (args, "-" + mnemonic);
			value = indexOfArg > -1;
			return value;
		}
	}
}

