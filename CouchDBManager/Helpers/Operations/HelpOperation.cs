﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CouchDBManager
{
	public class HelpOperation:OperationAbstract
	{
		string operation;

		public HelpOperation ()
		{
		}

		public override string Help ()
		{
			return "CouchDbManager help [OPERATION]";
		}

		public override Dictionary<string, string> ParamsDescription ()
		{
			return new Dictionary<string, string> () {
				{ "OPERATION","The operation of CouchDBManager. Example: CouchDbManager help delete" }
			};
		}

		public override int MinimumRequiredArgs ()
		{
			return 0;
		}

		public override void Execute ()
		{
			if (operation != null) {
				Startup.WriteCommandHelp ((OperationAbstract)Startup.operations [operation]);
			} else {
				foreach (DictionaryEntry op in Startup.operations) {
					Console.WriteLine ((op.Value as OperationAbstract).Help ());
				}
			}
		}

		public override bool ValidateArgs (string[] args)
		{
			operation = ParseStringArgs (args, 1);
			if (operation != null && !Startup.operations.Contains(operation)) {
				Console.WriteLine (string.Format("Invalid operation specified: {0}.",operation));
				return false;
			}
			return true;
		}
	}
}

