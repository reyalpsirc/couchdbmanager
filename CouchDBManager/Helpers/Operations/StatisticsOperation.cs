﻿using System;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace CouchDBManager
{
	public class StatisticsOperation:OperationAbstract
	{
		string host,database,path,username,password;
		public StatisticsOperation ()
		{
		}

		public override string Help ()
		{
			return "CouchDbManager statistics HOST DATABASE PATH [admin password]";
		}

		public override Dictionary<string, string> ParamsDescription ()
		{
			return new Dictionary<string, string> () {
				{ "HOST","The address where couchdb is running." },
				{ "DATABASE","The database to access." }, 
				{ "PATH", "The path that will return the documents to delete. This can be found from the urls on the browser. Example: _all_docs?include_docs=true"},
				{ "-u or --username","Username for authentication on the selected database" },
				{ "-p or --password","Password of the given user" }
			};
		}

		public override int MinimumRequiredArgs ()
		{
			return 3;
		}

		public override void Execute ()
		{
			CouchConnection con = new CouchConnection (host,database,username,password);

			//try to get a list of documents at the specified path
			JArray data = con.GetDocsListAtPath(path);
			if (data != null) {
				//if its not null, then the path contains a list of docs to delete
				if (data.Count > 0) {
					//convert JArray to List
					List<JObject> docsForStatistics=new List<JObject>();
					foreach (var d in data) {
						JObject theObj = null;
						//doc is used on _all_docs?include_docs=true
						if (d ["doc"] != null && d ["doc"].ToString () != "") {
							theObj = (JObject)d ["doc"];
						} else if (d ["value"] != null && d ["value"].ToString () != "") {
							theObj = (JObject)d ["value"];
						}
						if (theObj != null) {
							//add the doc to the array of docs for statistics
							docsForStatistics.Add(theObj);
						}
					}
					PrintStatistics (docsForStatistics);
				}
			} else {
				//let's check if the path contains one document only
				JObject singleDoc=con.GetDocAtPath(path);
				if (singleDoc != null) {
					//add the doc to the array of docs for statistics
					List<JObject> docsToDelete=new List<JObject>();
					docsToDelete.Add (singleDoc);
					PrintStatistics (docsToDelete);
				} else {
					Console.WriteLine ("No documents found at the specified path");
				}
			}
		}

		void PrintStatistics (List<JObject> docsForStatistics)
		{
			Dictionary<string,int> properties=new Dictionary<string, int>();
			int totalDesignDocs = 0;
			int biggestRevision = -1;
			foreach (var doc in docsForStatistics) {
				string id=doc["_id"].ToString();
				if (doc ["_rev"] != null && doc ["_rev"].ToString () != "") {
					string[] splitRev = doc ["_rev"].ToString ().Split('-');
					if (splitRev.Length > 0) {
						int revNumber = -1;
						int.TryParse (splitRev [0], out revNumber);
						if (revNumber > biggestRevision) {
							biggestRevision = revNumber;
						}
					}
				}
				if (id.StartsWith("_design/")){
					totalDesignDocs++;
				}
				foreach (var p in doc.Properties()) {
					if (properties.ContainsKey (p.Name)) {
						properties [p.Name]++;
					} else {
						properties [p.Name] = 1;
					}
				}
			}
			/**/
			Console.WriteLine ("First level properties found on the documents:");
			List<string> tagsSorted = properties.Keys.ToList();
			tagsSorted.Sort ();
			int totalTags = 0;
			foreach (var property in tagsSorted) {
				totalTags += properties [property];
				Console.WriteLine (string.Format ("    {0}: {1}",property,properties[property]));
			}
			Console.WriteLine (string.Format ("\nDesign documents: {0}", totalDesignDocs));
			Console.WriteLine (string.Format ("Other documents: {0}", docsForStatistics.Count-totalDesignDocs));
			Console.WriteLine (string.Format ("Total: {0}", docsForStatistics.Count));
			Console.WriteLine (string.Format ("\nBiggest revision found: {0}",biggestRevision));
			Console.WriteLine (string.Format ("Total first level distinct properties: {0}",tagsSorted.Count));
			Console.WriteLine (string.Format ("Total first level properties: {0}",totalTags));
		}

		public override bool ValidateArgs (string[] args)
		{
			host = ParseStringArgs(args,1);
			database = ParseStringArgs(args,2);
			path = ParseStringArgs(args,3);
			username = ParseStringArgs(args,"username","u");
			password = ParseStringArgs(args,"password","p");

			if (username!=null && password==null) {
				Console.WriteLine (string.Format("Please provide password for {0}.",username));
				return false;
			}
			return true;
		}
	}
}

