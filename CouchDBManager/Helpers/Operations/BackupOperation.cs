﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CouchDBManager
{
	public class BackupOperation:OperationAbstract
	{
		string destination,host,database,path,username,password;

		public BackupOperation ()
		{
		}

		public override string Help ()
		{
			return "CouchDbManager backup DESTINATION_FOLDER ALL|PATH HOST DATABASE [-u USER  -p PASSWORD]";
		}

		public override Dictionary<string, string> ParamsDescription ()
		{
			return new Dictionary<string, string> () {
				{ "DESTINATION_FOLDER", "The folder where the backup file will be saved."},
				{ "ALL|PATH","If you want to backup everything, just write ALL. Otherwise, specify the path on the server that will return the documents to backup." },
				{ "HOST","The address where couchdb is running." },
				{ "DATABASE","The database to access." }, 
				{ "-u or --username","Username for authentication on the selected database" },
				{ "-p or --password","Password of the given user" }
			};
		}

		public override int MinimumRequiredArgs ()
		{
			return 3;
		}

		public override void Execute ()
		{
			CouchConnection con = new CouchConnection (host,database,username,password);
			//get the docs from the couchdb server
			JArray data = con.GetDocsListAtPath (path);
			if (data.Count > 0) {
				Console.WriteLine (string.Format ("Confirm backup of {0} document(s)? Y/N", data.Count));
				string confirm = Console.ReadLine ();
				if (confirm.ToLower () == "y") {
					var docs=new JObject();
					docs ["docs"] = data;
					WriteBackup(destination,docs.ToString());
				}
			}

		}

		public override bool ValidateArgs (string[] args)
		{
			destination = ParseStringArgs(args,1);
			path = ParseStringArgs(args,2);
			if (path.Trim ().ToLower () == "all") {
				path = "_all_docs?include_docs=true";
			}
			host = ParseStringArgs(args,3);
			database = ParseStringArgs(args,4);
			username = ParseStringArgs(args,"username","u");
			password = ParseStringArgs(args,"password","p");

			if (username!=null && password==null) {
				Console.WriteLine (string.Format("Please provide password for {0}.",username));
				return false;
			}

			//confirm the destination folder exists
			if (!Directory.Exists (destination)) {
				Console.WriteLine ("Directory \""+destination+"\" does not exist");
				return false;
			}

			return true;
		}

		public void WriteBackup(string directory, string jsonStr){
			// Write the string to a file.
			if (directory [directory.Length - 1] != Path.DirectorySeparatorChar) {
				directory += Path.DirectorySeparatorChar;
			}
			string destination=directory+"bkp_"+DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")+".json";
			Console.WriteLine ("Backup at " + destination);
			if(!File.Exists(destination))
			{
				File.Create(destination).Dispose();
				using( TextWriter tw = new StreamWriter(destination))
				{
					tw.Write (jsonStr);
					tw.Close();
				}

			}

			else if (File.Exists(destination))
			{
				using(TextWriter tw = new StreamWriter(destination))
				{
					tw.Write (jsonStr);
					tw.Close(); 
				}
			}
		}
	}
}

