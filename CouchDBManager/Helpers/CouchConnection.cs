﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Text;

namespace CouchDBManager
{
	public class CouchConnection
	{
		private string host;
		private string database;
		private string username;
		private string password;

		public CouchConnection (string host, string database):this(host,database,null,null)
		{
		}

		public CouchConnection (string host, string database, string username, string password)
		{
			this.host = ParseHost(host);
			this.database = database;
			this.username = username;
			this.password = password;
		}

		/// <summary>
		/// Converts the host string to an url if it is an IP Address
		/// </summary>
		/// <returns>The formatted host string.</returns>
		/// <param name="host">The host in a formatted or non formatted format (ex: IP Address).</param>
		private string ParseHost(string host){
			string parsedHost = host;
			if (!parsedHost.Contains ("://")) {
				parsedHost = "http://" + host;
			}
			if (parsedHost [parsedHost.Length - 1] != '/') {
				parsedHost += "/";
			}
			return parsedHost;
		}

		/// <summary>
		/// Creates the default HttpClient object used on requests
		/// </summary>
		/// <returns>The default HttpClient used on requests.</returns>
		private HttpClient GetDefaultClient (){
			HttpClient client = new HttpClient ();
			if (username != null && password!=null) {
				String encoded = System.Convert.ToBase64String (System.Text.Encoding.GetEncoding ("ISO-8859-1").GetBytes (username+":" + password));
				client.DefaultRequestHeaders.Add ("Authorization", "Basic " + encoded);
			}
			client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
			client.DefaultRequestHeaders.Add ("Accept-Charset", "utf-8");
			return client;
		}

		/// <summary>
		/// Returns the single document tha exists on the specified path of the CouchDB server. The path is what appears next
		/// to the database name on the url accessed via browser.
		/// </summary>
		/// <returns>The single document returned by the path.</returns>
		/// <param name="path">The path to access on the CouchDB server.</param>
		public JObject GetDocAtPath(string path){
			//grab all documents
			HttpClient client=GetDefaultClient();
			string theHost = host;
			string mainUrl = theHost + database + "/"+NormalizePath(path);
			try{
				var t = client.GetAsync (new Uri (mainUrl));
				t.Wait();
				if (!t.Result.IsSuccessStatusCode) {
					Console.WriteLine ("FAILED: " + t.Result);
					return null;
				}
				HttpResponseMessage request = t.Result;
				var t2=request.Content.ReadAsStringAsync ();
				t2.Wait ();
				string resp = t2.Result;
				JObject json = JObject.Parse (resp);
				if (json["_id"]!=null && json["_rev"]!=null){
					return json;
				} else {
					return null;
				}
			} catch (Exception ex){
				Console.WriteLine ("A general error occured. Is the host and the port correct?");
			}
			return null;
		}

		/// <summary>
		/// Gets the documents list at a specified path on the CouchDB server. The path is what appears next
		/// to the database name on the url accessed via browser.
		/// </summary>
		/// <returns>The documents list.</returns>
		/// <param name="path">The path to access on the CouchDB server.</param>
		public JArray GetDocsListAtPath(string path){
			JArray existentDocs = new JArray ();
			//grab all documents
			HttpClient client=GetDefaultClient();
			string theHost = host;
			string mainUrl = theHost + database + "/"+NormalizePath(path);
			try{
				var t = client.GetAsync (new Uri (mainUrl));
				t.Wait();
				if (!t.Result.IsSuccessStatusCode) {
					Console.WriteLine ("FAILED: " + t.Result);
					return existentDocs;
				}
				HttpResponseMessage request = t.Result;
				var t2=request.Content.ReadAsStringAsync ();
				t2.Wait ();
				string resp = t2.Result;
				JObject json = JObject.Parse (resp);
				if (json ["rows"] != null) {
					existentDocs = (JArray)json ["rows"];
				} else {
					return null;
				}
			} catch (Exception ex){
				Console.WriteLine ("A general error occurred. Is the host and the port correct?");
			}
			return existentDocs;
		}

		/// <summary>
		/// Removes the first slash of the path if it is found at the start of it.
		/// </summary>
		/// <returns>The path.</returns>
		/// <param name="path">The normalized path.</param>
		private string NormalizePath (string path)
		{
			if (path!=null && path [0] == '/') {
				path = path.Substring(1);
			}
			return path;
		}

		/// <summary>
		/// Gets the id and the latests rev of all the documents that exist on the couchdb database.
		/// If includeDocs=true, it will also return the data of each document for the selected revision
		/// </summary>
		/// <returns>Key and rev of all the documents on the couchdb database.</returns>
		/// <param name="includeDocs">If set to <c>true</c>, it will include the data of each document.</param>
		public JArray GetExistentDocs(bool includeDocs=false){
			string path = "_all_docs";
			if (includeDocs) {
				path += "?include_docs=true";
			}
			return GetDocsListAtPath(path);
		}

		/// <summary>
		/// Gets the latest rev for each one of the documents of the couchdb database.
		/// Each key of the dictionary corresponds to a document id and the value is the latest rev of that document.
		/// </summary>
		/// <returns>A dictionary with the latest revision for each document id on the couchdb database</returns>
		public Dictionary<string,string> GetExistentIdsWithRevs(){
			var docs = GetExistentDocs ();
			Dictionary<string,string> result = new Dictionary<string, string>();
			foreach (var item in docs) {
				if (item["id"]!=null && item["id"].ToString()!=""){
					if (item ["value"] != null && item ["value"].ToString () != "") {
						if (item ["value"]["rev"] != null && item ["value"]["rev"].ToString () != "") {
							result [item ["id"].ToString ()] = item ["value"]["rev"].ToString ();
						}
					}
				}
			}
			return result;
		}

		/// <summary>
		/// Updates or inserts a bulk of documents. If some documents fail due to conflicts or something else, they
		/// are added to a BulkFails object which is returned at the end
		/// </summary>
		/// <returns>A BulkFails object which contains the documents that failed to insert/update.</returns>
		/// <param name="docsToSave">The documents to save (insert or update).</param>
		public BulkFails BulkSend (List<JObject> docsToSave)
		{
			//Prepare the list of docs to send as post to the bulk url
			Dictionary<string,JObject> docsMap=new Dictionary<string, JObject>();
			BulkFails bulkResult = new BulkFails ();
			JObject rootObj=new JObject();
			JArray docs = new JArray ();
			rootObj.Add ("docs",docs);
			foreach (var item in docsToSave) {
				if (item ["_id"] != null && item ["_id"].ToString () != "") {
					var docId = item ["_id"].ToString ();
					docsMap [docId] = item;
					docs.Add (item);
				}
			}

			//make the request for the couch server
			HttpClient client = GetDefaultClient ();
			string theHost = ParseHost(host);
			string mainUrl = theHost + database + "/_bulk_docs";
			try{
				var t = client.PostAsync (new Uri (mainUrl),new StringContent(rootObj.ToString(),Encoding.UTF8, "application/json"));
				t.Wait();
				if (!t.Result.IsSuccessStatusCode) {
					Console.WriteLine ("FAILED: " + t.Result);
					return null;
				}
				HttpResponseMessage request = t.Result;
				var t2=request.Content.ReadAsStringAsync ();
				t2.Wait ();
				string resp = t2.Result;
				JArray resultArray = JArray.Parse (resp);
				if (resultArray != null && resultArray.Count>0) {
					//check for failed docs and add them to the BulkFails instance
					foreach (var item in resultArray) {
						if (item ["id"] != null && item ["id"].ToString () != "") {
							var docId = item ["id"].ToString ();
							if (item ["error"] != null && item ["error"].ToString () != "") {
								if (item ["error"].ToString ()=="conflict"){
									bulkResult.ConflictedDocs.Add(docsMap[docId]);
								} else {
									if (item ["reason"] != null && item ["reason"].ToString () != ""){
										bulkResult.OtherIdsErrors.Add(new BulkFails.OtherError(docsMap[docId],item ["reason"].ToString ()));
									} else {
										bulkResult.OtherIdsErrors.Add(new BulkFails.OtherError(docsMap[docId],"A general error happened."));
									}
								}
							}
						}
					}
				}
			} catch (Exception ex){
				Console.WriteLine ("A general error occured. Is the host and the port correct?");
			}
			return bulkResult;
		}

		public void RetryConflictsOnBulkSend(Func<List<JObject>> beforeSend,Action<BulkFails> onFail, int maxRetries=5){
			int count = 0;
			BulkFails failResults = null;
			do {
				var docsToSave=beforeSend();
				failResults=BulkSend(docsToSave);
				if (failResults!=null){
					if ((failResults.ConflictedDocs!=null && failResults.ConflictedDocs.Count>0) || 
						(failResults.OtherIdsErrors!=null && failResults.OtherIdsErrors.Count>0)){
						onFail(failResults);
					} 
				}
				count++;
			} while(failResults!=null && failResults.ConflictedDocs!=null && failResults.ConflictedDocs.Count>0 && count<=maxRetries);
		}

		/// <summary>
		/// Gets the current data of the specified document id.
		/// </summary>
		/// <returns>The data of the document.</returns>
		/// <param name="id">The id of the document to retrieve.</param>
		public JObject GetCurrentDoc (string id)
		{
			try {
				//Get the rev of this document and do a normal update
				HttpClient client = GetDefaultClient ();
				string theHost = ParseHost (host);
				string mainUrl = theHost + database + "/" + id;
				var t = client.GetAsync (new Uri (mainUrl));
				t.Wait ();
				//only set the revision on the doc if the document previously exist
				if (t.Result.IsSuccessStatusCode) {
					HttpResponseMessage request = t.Result;
					var t2 = request.Content.ReadAsStringAsync ();
					t2.Wait ();
					string resp = t2.Result;
					return JObject.Parse (resp);
				}
			} catch (Exception ex) {
				Console.WriteLine (ex);
			}
			return null;
		}
	}
}

