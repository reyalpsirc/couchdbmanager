﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace CouchDBManager
{
	public class BulkFails
	{
		public List<JObject> ConflictedDocs;
		public List<OtherError> OtherIdsErrors;

		public BulkFails ()
		{
			ConflictedDocs = new List<JObject> ();
			OtherIdsErrors = new List<OtherError>();
		}

		public class OtherError{
			public JObject Doc;
			public string Error;

			public OtherError (JObject doc, string error)
			{
				this.Doc = doc;
				this.Error = error;
			}
			
		}
	}
}

