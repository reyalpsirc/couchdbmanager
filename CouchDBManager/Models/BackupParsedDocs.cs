﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace CouchDBManager
{
	public class BackupParsedDocs
	{
		public List<JObject> normalDocs;
		public List<JObject> designDocs;

		public BackupParsedDocs ()
		{
			normalDocs=new List<JObject>();
			designDocs=new List<JObject>();
		}
	}
}

